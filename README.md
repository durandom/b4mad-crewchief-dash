# B4mad-CrewChief-dash



## Getting started

i'm going to save time in the documentation because this are extra features of [#B4mad racing](https://b4mad.racing) which all depend of [CrewChief](https://thecrewchief.org/).

how to start [B4Mad Read ME](https://github.com/b4mad/racing/blob/main/README.md)

All start by i was looking a way to control some lights in [Homeassistant](https://www.home-assistant.io/) with my flag information from the game, i found B4Mad project use MQTT, but sadly didn't had Flag info in the payload, then i found out we can create our own and as i'm a data-junkie i start building my own telemery dashboard. 

Your telemetry data will be availabe in [B4mad Pitwall](https://pitwall.b4mad.racing/) and your Local 

## What is the extra
First born as a local project 
i had a local 
- MQTT
- Grafana 
- Influx (both versions)
- Telegraf (2 versions too)

My crewchief data is been send to my Local MQTT
My Local MQTT is been consume by Homeassistant and Telegraf
The First Telegraf process my data and send it to my 2 influxdb (are 2 because i start in v1.7 and B4mad use Flux)
here i can transform as i wish and create my local dashboards or use more in homeassistant (more later)

Then the other Telegraf i called Relay 
is connect direct to my mosquito collect the raw topic 
do some magic to re send the data to B4mad MQTT server 

Down side is that you may lost access to his coach.

## Ideas
As is comunity oriented i think why we instead of just "User name" we may add more data to that like "User Name"_ CountryCode _"Team Name" (without quoutes but allowing spaces)
so then the telegraf can collect those and build some tables around (done in my local)

still thinking how to create visuals of position and forces (no all games had data)

## Bugs on B4

i'm noticing some panels are not working 


## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
[CrewChief](https://gitlab.com/mr_belowski/CrewChiefV4) this tool is what makes everything posible  

[B4mad](https://github.com/b4mad/racing)


## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
